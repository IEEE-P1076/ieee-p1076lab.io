<!-- Use to following title

2022-Month-Day Meeting

-->

# Attendees

<!---
- Jim Lewis (@JimLewis)
- Patrick Lehmann (@paebbels)
- Peter Flake (@pflake)
- Marlon James (@marlondjames)
- Pablo Blecua (@pblecua)
- Unai Martinez-Corral (@umarcor)
-->

# Meeting Discussion

* *TBD*

# Next Meeting

{-Day-} {-Month-} 2022, 11 am Pacific Standard Time (GMT-8)

## Tentative Agenda

* [Issues](https://gitlab.com/groups/IEEE-P1076/-/issues?label_name%5B%5D=Next+Meeting) | [Merge Requests](https://gitlab.com/groups/IEEE-P1076/-/merge_requests?label_name%5B%5D=Next+Meeting)

<!-- automatically applied issue settings -->
/label ~"Meeting::Agenda"
