Language Change Workflow
########################

The management of VHDL is undergoing a migration from an industry driven and closed source model, into adopting some
open source development practices.
During the previous revision of the language, most of the work was coordinated through TWIKI, and by the end the
activity was moved to GitLab.
Conversely but coherently, for the next 202X release, the source of truth will be the GitLab repositories.

Initial discussion
==================

Initially, any of the existing channels is suitable for discussing a feature, bug or enhancement.
Other members of the group might help providing similar references and feedback.
The goal of the initial discussion is to maximize the exposure of the question, so that as many people as possible can
be aware.

.. NOTE::
  Some discussions can be initialized as Issues, which then evolve into Proposals.

Issues
======

Issues are used for getting feedback about implementation/interpretation issues, for documenting discussions, and/or for
formalizing bug fixes, feature requests, and/or proposals.

Bug Report
----------

Unfortunately, several typo/edition issues were introduced during the edition of the 2019 LRM, or were not fixed from
previous revisions.
These are LCSs in an strict sense, because they imply modifying the LRM and/or the libraries.
However, thorough discussion/analysis is not required, because they seek to make the sources match previous agreements.

Capability or Feature Request
-----------------------------

A capability or feature request is an abbreviated form of a proposal.
Frequently, we find ourselves seeking a feature, without properly analyzing the capabilities that we are trying to
gain through it.
Hence, it is strongly suggested to explain *what* are we trying to achieve, before we address *how* we wish to achieve
it.

Contributors might optionally write some Merge Request for showcasing the desired feature, before the proposal is
complete.
However, a feature request **must** be formalized into a proposal before it is evaluated as a Language Change Specification
(LCS).

Formal Proposal
---------------

The goal of a proposal is to formalize a discussion.
That is, define which is the feature/bug, along with explanations about how it works or should work, but not how it maps
to the LRM.
Identifying the purpose and providing code examples (before and after) are the outcomes of a proposal.
An analysis of impact of change (from 2008 LCS/Proposals) might be included.

If the proposal is not tracked as an issue in repo `VHDL Issues <https://gitlab.com/IEEE-P1076/VHDL-Issues/-/issues>`__,
it should be created and labeled.
Otherwise, add reactions and/or comments to existing issues in order to let others know about the importance and/or
about relevant details for your use case.
Design decisions are driven by usability and interest from the user community.

.. IMPORTANT::
  For each proposal which is currently only tracked in `TWIKI: VHDLProposals <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/VHDLProposals>`__,
  an issue needs to be created in repository VHDL Issues.
  It is acceptable to have issues with no matching wiki page, but not otherwise.
  This is because GitLab labels and milestones are used for tracking and organising the work.

Merge requests
==============

GitLab Merge Requests (MRs) are used for Repository Chores, Bug Fixes, or Language Change Specifications (LCSs).

Repository Chores
-----------------

Any modification to the repositories which does not modify the LRM or the VHDL libraries is considered a chore.
These can be merged by repository maintainers, without explicit approval by the group.

Bug Fix
-------

All merge requests addressing some obvious and minor issue are subject to be picked by a maintainer in a trivial fixes
branch.
For these, the body of an issue can be considered a complete proposal.
The working group will review trivial fixes during meetings in order to decide moving them to the development branch.

Language Change Specification
-----------------------------

After a proposal is discussed, elaborated and detailed, a Language Change Specification (LCS) needs to be *written*.
An LCS is an specific implementation of a proposal.
In practice, *writing* implies preparing one or multiple Merge Requests that modify the LRM-LaTeX and/or Packages
repositories.

Each LCS **must** be linked to a complete proposal so that a modification report can be generated and evaluated by the group.
Furthermore, LCSs might be kept on hold until the Project Authorization Request (PAR), since the PAR defines the scope
of the revision.

Modifications
=============

In practice, multiple LCSs might be proposed for a single modification proposal. By the same token, a single LCS might
address multiple modification proposals. In order to handle this ambiguity, a *modification* is a group of issues and
merge requests which belong to the same fix or enhancement. Upon voting, members of the group analyze each modification
as a whole.

Modifications are tracked through labels in GitLab repositories. For each discussed and approved modification, an
identifier is defined for grouping the related items. Modifications and their related issues and merged requests are
shown in :ref:`VHDL202X:Modifications`. Tables are generated through GitLab's API when this site is updated; hence,
there might be some delay.

.. ATTENTION::
  There is common misunderstanding about the driving force behind the working group.
  Being a volunteer driven working group, there is no guarantee for incomplete proposals to be picked by others.
  The group is not reponsible for writing MRs for each Issue or writing Issues for working but isolated MRs.
  It is up to contributors to properly complete and match proposals and MRs before they are evaluated by the working
  group for merging into the development branch.
  Naturally, experienced members of the working group are expected to help new contributors writing proposals and LCSs,
  however, that is on a best effort basis.
