.. _VHDL202X:Meetings:

Meetings
########

Meeting Days and Minutes
========================

Propose topics for the next meeting(s) by replying to the `next Meeting::Agenda <https://gitlab.com/IEEE-P1076/ieee-p1076.gitlab.io/-/issues?label_name%5B%5D=Meeting%3A%3AAgenda>`__.
or, marking some issue in any other repository through `label <https://gitlab.com/groups/IEEE-P1076/-/labels>`__ *Next Meeting*.
See `Issues <https://gitlab.com/groups/IEEE-P1076/-/issues?label_name%5B%5D=Next+Meeting>`__ and `Merge Request <https://gitlab.com/groups/IEEE-P1076/-/merge_requests?label_name%5B%5D=Next+Meeting>`__ marked already.

Future Meeting(s)
-----------------

.. exec::
  from GitLabAPI import future_meetings
  future_meetings()

.. IMPORTANT::
  * Usual meeting time: Tuesday at 11 am Pacific US Standard Time
  * See `dial-in information <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/DialInInfo>`__.

Minutes for Past Meetings
-------------------------

.. NOTE::
  Prior to using this page, `Vhdl202X_MeetingMinutes <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl202X_MeetingMinutes>`__ was used for tracking next meeting information, minutes, dial-in information, etc.

.. exec::
  from GitLabAPI import meeting_minutes
  meeting_minutes()
