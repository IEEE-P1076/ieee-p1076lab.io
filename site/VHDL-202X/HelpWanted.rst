.. _VHDL202X:HelpWanted:

Help Wanted
###########

This is a public page for Working Group members to detail where help is needed.
Please include a description of the work and provide contact information.

This page can be edited here: https://gitlab.com/IEEE-P1076/ieee-p1076.gitlab.io

Covert LRM to LaTeX
===================
The LRM has been converted to LaTeX, but needs proof-readers
to validate it matches VHDL-2019
(without the typographical errors introduced by tooling).

The GitLab repo is currently private. Please contact the Working Group
through one of the channels in :ref:`Participating`.

This is an issue for tracking the validation of the different parts of the LRM:
https://gitlab.com/IEEE-P1076/lrm-latex/-/issues/12

