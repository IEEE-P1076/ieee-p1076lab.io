Documentation Tool Requirements
###############################

Committee members from the last round (VHDL-2019) remember the disaster and headache of the final editing of the spec in
FrameMaker.
To be specific, the FrameMaker application crashed roughly every 20 minutes on our complex document.
And the editors discovered after some time that when it crashed it also corrupted the document!

This page documents the requirements, process and decision of a new documentation tool for the VHDL-202x revision cycle.

Requirements
============

* The documentation format should allow many people should be able to contribute simultaneously with minimal effort.
* The documentation format should be revision controllable using standard tools (currently git/GitLab).
* The documentation format should be robust.

Derived requirements and discussion and conclusions
===================================================

* Text-based formats are inherently robust against tool malfunction. Even if the transformation tool (Org, reST, LaTeX,
  whatever) fails spectacularly when rendering your document, your original text file is open in read mode.
  So you don't ever lose anything.
  And any changes will show up in the revision history.
* Text-based formats generally provide for the simplest revision control.
  Most binary/proprietary document formats require specialized version control if it is available at all.
* Text-based formats usually allow for arbitrary line breaks.
  Adding a word and re-justifying a paragraph may greatly complicate the comparison and merge process.
  Can we configure the hosting tool to ignore (most) white space?
* Internal revision control systems like MS Word allow for comments "inline" or directly connected to the source
  document in order to ask questions, discuss, or explain actions.
  This can be a nice feature, especially for small teams.
  Similar metadata for a text-based format would require very disciplined issue creation and commit comments.
* To one author's knoledge, GoogleDocs and other online authoring services don't offer difference-based revision
  control, so these options are not considered.

Format competition
==================

For your preferred format, please provide a document that includes a handful of representative pages for transformation.
We will setup a CI flow for each format on GitLab and compare the following.

* complexity of input
* expressiveness of outputs compared to original LRM
* how to compare 2 versions

This experiment will ignore for now:

* styling of final template to try to match the LRM formatting exactly, and
* time to write a template since this is a one-time job.

Please include pages with the following features:

* a page with multiple source code snippets
* a pages with footnotes
* a page with deeply nested lists (4..6 levels)
* two separate sections of the attributes table (such as 16.2.2 and 16.2.3) with identical column widths (vs. only
  autosized tables)
* a table of contents including several levels of document headings
* some images / diagrams e.g. from VHPI
* one page of EBNF rules
