.. _VHDL202X:Modifications:

Modifications
#############

.. NOTE::
  `Proposals & Requirements List backlog from 1076-2019 <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/VHDLProposals>`__
  (do NOT add new proposals there)

.. ATTENTION::
  This is an experimental feature!
  GitLab's GraphQL API is used for retrieving the issues from IEEE-P1076/VHDL-Issues, then filtering them in Python and
  generating restructuredtext tables with ``tabulate``.
  However, the logic for actually tracking modification groups was not implemented yet, because no group was yet defined
  for VHDL-202X.
  For now, the issues corresponding to some specific labels are shown only.

.. exec::
   from GitLabAPI import modifications
   modifications()
