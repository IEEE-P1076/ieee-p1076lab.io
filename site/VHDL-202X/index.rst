.. _VHDL202X:

VHDL-202X
#########

* `Submitted PAR Request for VHDL 202X (12 Oct 2021) <http://www.eda-twiki.org/twiki/pub/P1076/PrivateDocuments/Par1076_202X.pdf>`__.
  The Expected Date of submission of draft to the IEEE SA for Initial Standards Committee Ballot is Jun 2025.

.. toctree::
    :maxdepth: 1

    Meetings
    HelpWanted
    LanguageChangeWorkflow
    Modifications
    DocumentationToolRequirements

* Documents

  * `Private Documents <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/PrivateDocuments>`__
  * `Public Documents <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/PublicDocuments>`__
  * `Old Public Documents <http://www.eda-twiki.org/vasg/docs/>`__
