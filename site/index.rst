IEEE P1076 Working Group: VHDL Analysis and Standardization Group (VASG)
########################################################################

.. raw:: html

  <p style="text-align: center; line-height: 33px; font-size: 24px">
   IEEE P1076 Working Group
   </br>
   VHDL Analysis and Standardization Group (VASG)
  </p>

.. only:: html

    .. |shieldgitlab| image:: https://img.shields.io/badge/-IEEE--P1076-323131.svg?logo=gitlab&style=flat-square&longCache=true
    .. _shieldgitlab: https://gitlab.com/IEEE-P1076

    .. |shieldreflector| image:: https://img.shields.io/badge/-grouper.ieee.org/groups/1076-323131.svg?logo=ieee&style=flat-square&longCache=true
    .. _shieldreflector: http://grouper.ieee.org/groups/1076/email

    .. |shieldtwiki| image:: https://img.shields.io/badge/-eda--twiki.org-323131.svg?logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA%2FwD%2FAP%2BgvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QIWAhImtwMY5AAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACwUlEQVRYw%2B1XTWgTQRT%2BZpOokURDg6GiaAwoCEX8QQpevFTQHgTBY8FrERSstKLePVSleLSH3qUFRSx48CR4CIJU8BC0WKqVULQiJsZmZ94%2BD83GJPOTDR68dGHZXYb3vu99882bWWDz%2Bs%2BXCF%2F408Au%2BD9GoaqXoIICFAQUAAWAGk%2FXTQCkAFSwhiAxD94%2BJS6vLEQiwOULp1B5Og9JGS2xbCEgDcDheOcYxQhb%2BibF2NdbTgL8%2BWQOv98sQlJaSyQ7qpeW6k0kQkWyR86Jq2%2Bf2wh4qK%2BMdgWXBnBpeaoWMpKB1dIztwKl9BL8Sl4DF6lXkMGyltT%2BruCrs%2FD9nJYre%2FCMuP3hhYlAHH5tvz5%2FAJL5B%2BL0u9leHM13My8h%2FZyWq1ot2KdAsdACFAAVxHpeUzZ%2F1Nc9BwHD3IXf%2F0og9A4Ja0jcCC4B8PfjPJuqNsfFtkUx8q3UlJvZw730eRBUI2YdvuzTTBrmdRIggwfqq%2BNQGG%2BuBtAkgBstsTHUfz1ur5j1FSEBJBBBAW2Nc8d0cKBFh4BdO6SLADk6XOs392A61YsCUcBVD6YzKeAwtOfs%2FW1O7kJAWjYtW6zmAVcFBCBwEJBdPKCimpAcVQkLARlxu3YSkBZg6pLAthWbVHWakAwHj04fmJopORSI2FXNCpBBjaBHBWQEBTf6gAdQ4PaATQFlUa6TmE9sJ4DkEmTlgBOcABD385XUMZAQUGBcP5To2v3CnIE3DOAhXxwUYq7I7QeS6d038bN8xwm%2B0QsYBO4Y89o2Hd%2BigAQwMDgl5opjeiOKZ6fBW9esFfxtRAISXgPUawO3kW7N97p4jQ%2FvfMQjw%2Fv0U%2FHM0X6U339BreZZk5gAQol9B7huygB7CvfFwseJ9v%2BCmaEMlosTUBhCfMcJqJY2TQ7DKccyNHVUBcAPgESSsDf%2FZPPXbPP6A13nhms8bYukAAAAAElFTkSuQmCC&style=flat-square&longCache=true
    .. _shieldtwiki: http://www.eda-twiki.org/cgi-bin/view.cgi/P1076

    .. centered:: |shieldgitlab|_ |shieldreflector|_ |shieldtwiki|_

Mission
=======

The VHDL Analysis and Standardization Group (VASG) is responsible for maintaining and extending the VHDL standard (IEEE
1076), which is delivered as a Language Reference Manual (LRM) along with Packages (standard and IEEE libraries).

Status
======

The Working Group is starting to identify issues to work on for the next 202X revision of the standard, in order to prepare
a Project Authorization Request (PAR).

.. _Participating:

Participating
=============

If you are a VHDL user, digital designer, or verification engineer, then this is your working group.

P1076 is an individual based standard.
The only requirement for membership is to show up through any of the communication channels, and participate.
There are no fees required.

.. _TWIKI: http://www.eda-twiki.org/cgi-bin/view.cgi/P1076

Work is done in
on-line :ref:`VHDL202X:Meetings`,
:ref:`Participating:Email`,
:ref:`Participating:Chat` (Gitter),
GitLab :ref:`Participating:Repositories` (newer stuff),
`TWIKI`_ site (older stuff),
etc.
All of this permits you to contribute what you can when you can.

We seek volunteers experienced in one or more:

* (V)HDL design and/or verification
* Language design (VHDL, Ada, C, Python...)
* Programming Language Interfaces (VHPI, VPI, DPI, FFI...)
* Digital design experience (DSP, Floating-Point, Machine-Learning...)
* LaTeX
* GitLab API, Sphinx, Continuous Integration...

We need all skills, including users who can say *'I will use that feature if you make it!'*.
While we always need experienced VHDL users to participate, currently we also need LaTeX users to help out.

If you are interested in contributing, see the :ref:`VHDL202X:HelpWanted` page for current needs.

Moreover, we would like any VHDL user to `submit enhancement requests, proposals, or bug reports for 202X <https://gitlab.com/IEEE-P1076/VHDL-Issues/-/issues>`_.
Join us and help create the next VHDL standards!

.. _Participating:Email:

Email Reflector/List
--------------------

  * Archive: `grouper.ieee.org/groups/1076/email <http://grouper.ieee.org/groups/1076/email/thread.html>`__
  * Subscribe `mailto:ListServ <mailto:ListServ@ieeeNOSPAM.org?body=subscribe VHDL 200X Your Name>`__ (remove NOSPAM and
    Update Your Name)
  * Unsubscribe `mailto:ListServ <mailto:ListServ@ieeeNOSPAM.org?body=unsubscribe vhdl-200x>`__
  * Send Email: `mailto:vhdl-200x <mailto:vhdl-200x@ieeeNOSPAM.org>`__ (subscribe first)
  * `Original EDA.org Reflector <http://www.eda-twiki.org/vhdl-200x/hm/>`__ (ended in 2016)

.. _Participating:Chat:

Chat rooms
----------

The GitHub organization named `Open Source VHDL Group (OSVG) <https://github.com/VHDL>`__ maintains a set of chat rooms in
`gitter.im/VHDL <https://gitter.im/VHDL>`__:

* `gitter.im/VHDL/P1076-WG <https://gitter.im/vhdl/P1076-WG>`__: private chat room for the IEEE P1076 Working Group.
* `gitter.im/VHDL/General <https://gitter.im/vhdl/General>`__: discussions about the VHDL language, VHDL tools chains and
  frameworks.
* `gitter.im/VHDL/Interfaces <https://gitter.im/vhdl/Interfaces>`__: discussions about VHDL-2019 interfaces
  (see `github.com/VHDL/Interfaces <https://github.com/VHDL/Interfaces>`__).

.. _Participating:Repositories:

Repositories
------------

* Releases of VHDL libraries are published at `opensource.ieee.org/vasg/Packages <https://opensource.ieee.org/vasg/Packages>`__
  (since December 2019)
* In order to reach a wider audience, the VASG uses an organization in GitLab: `gitlab.com/IEEE-P1076 <https://gitlab.com/IEEE-P1076>`__

.. only:: html

    .. |imgIssues| image:: _static/img/General-Issues.png
    .. _imgIssues: https://gitlab.com/IEEE-P1076/VHDL-Issues
    .. _Issues: https://gitlab.com/IEEE-P1076/VHDL-Issues

    .. |imgPackages| image:: _static/img/Library.png
    .. _imgPackages: https://gitlab.com/IEEE-P1076/Packages
    .. _Packages: https://gitlab.com/IEEE-P1076/Packages

    .. |imgLRM| image:: _static/img/LaTeX.png
    .. _imgLRM: https://gitlab.com/IEEE-P1076/LRM-LaTeX
    .. _LRM: https://gitlab.com/IEEE-P1076/LRM-LaTeX

    .. table::
        :align: center
        :widths: 100 100 100

        ============ ============== =========
        `Issues`_    `Packages`_    `LRM`_
        ============ ============== =========
        |imgIssues|_ |imgPackages|_ |imgLRM|_
        ============ ============== =========

.. NOTE::
  The master branch from `opensource.ieee.org/vasg/Packages <https://opensource.ieee.org/vasg/Packages>`__ is mirrored
  into `gitlab.com/IEEE-P1076/Packages <https://gitlab.com/IEEE-P1076/Packages>`__.
  The development is coordinated in branch ``develop`` of the latter, which is to be pushed to the ``release`` of the
  former when the next revision is approved.

TWIKI
-----

To participate in `TWIKI`_, get in touch with the officers (see :ref:`About:Admin`).


.. toctree::
   :hidden:

   About/index
   Contributing towards VHDL-202X <VHDL-202X/index>
   Prior
