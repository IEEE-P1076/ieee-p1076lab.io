#!/usr/bin/env python3

# Copyright 2021 Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import requests
from textwrap import shorten
from urllib.parse import quote
from tabulate import tabulate

# https://docs.gitlab.com/ee/api/#graphql-api
# https://docs.gitlab.com/ee/api/graphql/index.html
# https://docs.gitlab.com/ee/api/graphql/#graphiql
# https://docs.gitlab.com/ee/api/graphql/getting_started.html

query_VHDLIssues = """
    {
      project(fullPath: "IEEE-P1076/VHDL-Issues") {
        issues {
          nodes {
            iid
            title
            state
            labels {
              nodes {
                  title
                }
            }
            upvotes
            downvotes
          }
        }
      }
    }
    """

query_MeetingMinutes = """
    {
      project(fullPath: "IEEE-P1076/ieee-p1076.gitlab.io") {
        issues(labelName:"Meeting::Minutes", sort: CREATED_DESC) {
          nodes {
            iid
            title
          }
        }
      }
    }
    """

query_FutureMeetings = """
    {
      project(fullPath: "IEEE-P1076/ieee-p1076.gitlab.io") {
        issues(labelName:"Meeting::Agenda", sort: CREATED_DESC) {
          nodes {
            iid
            title
          }
        }
      }
    }
    """


def doQuery(query):
    """
    Uses GitLab's GraphQL API for retrieving lists of issues.
    """
    request = requests.post(
        "https://gitlab.com/api/graphql",
        # The query below can be pasted into the GraphiQL explorer (https://gitlab.com/-/graphql-explorer)
        # for viewing the request response (the data in JSON format)
        # Find other available field in the reference (https://docs.gitlab.com/ee/api/graphql/reference/index.html)
        json={"query": query},
        # headers={"Authorization": "token YOUR API KEY"}
    )
    if request.status_code != 200:
        raise Exception(f"Query failed: {request.status_code}")

    return request.json()["data"]


def getIssues():
    """
    Uses GitLab's GraphQL API for retrieving the list of issues in IEEE-P1076/VHDL-Issues.
    """
    # TODO: Should use a more structured solution, rather than an array of 5 elements
    # ID: int
    # Upvotes: int
    # Downvotes: int
    # Title: str
    # Labels: List[str]
    return [
        [
            issue["iid"],
            issue["upvotes"],
            issue["downvotes"],
            issue["title"],
            [label["title"] for label in issue["labels"]["nodes"]],
        ]
        for issue in doQuery(query_VHDLIssues)["project"]["issues"]["nodes"]
    ]


def printIssueTable(content, headers):
    """
    Uses tabulate for printing an rst table from a list of lists.
    """
    print(
        tabulate(
            [
                [
                    item[0],
                    item[1],
                    item[2],
                    # Convert the title into a link to the issue
                    f":vhdlissue:`{shorten(item[3], width=50)} <{item[0]}>`",
                    # Convert the labels into comma separated links to them
                    ", ".join(
                        f":vhdllabel:`{label} <{quote(label)}>`" for label in item[4]
                    ),
                ]
                for item in content
            ],
            headers=headers,
            tablefmt="rst",
        )
    )
    print()


def isInScope(scope, labels):
    """
    Check if any of the labels in a list belong to the given scope
    """
    for label in labels:
        if label.startswith(scope):
            return True
    return False


def modifications():
    """
    Get the issues from the GitLab API and generate multiple filtered rst tables.
    """
    issues = getIssues()

    print("All")
    print("===")

    printIssueTable(issues, headers=["ID", "Up", "Down", "Title", "Labels"])

    print("Enhancements")
    print("============")

    printIssueTable(
        [issue for issue in issues if "Enhancement" in issue[4]],
        headers=["ID", "Up", "Down", "Title", "Labels"],
    )

    print("Bugs")
    print("====")

    bugs = [issue for issue in issues if isInScope("Bug::", issue[4])]

    print("Confirmed")
    print("---------")

    printIssueTable(
        [bug for bug in bugs if "Bug::Confirmed" in bug[4]],
        headers=["ID", "Up", "Down", "Title", "Labels"],
    )

    print("Needs confirmation")
    print("------------------")

    printIssueTable(
        [bug for bug in bugs if "Bug::Needs Confirmation" in bug[4]],
        headers=["ID", "Up", "Down", "Title", "Labels"],
    )

    print("APIs")
    print("====")

    apis = [issue for issue in issues if isInScope("API:", issue[4])]

    print("VHPI")
    print("----")

    printIssueTable(
        [api for api in apis if "API: VHPI" in api[4]],
        headers=["ID", "Up", "Down", "Title", "Labels"],
    )


def meeting_minutes():
    """ """
    for issue in doQuery(query_MeetingMinutes)["project"]["issues"]["nodes"]:
        print(f"* :meeting:`{issue['title']} <{issue['iid']}>`")


def future_meetings():
    """ """
    for issue in doQuery(query_FutureMeetings)["project"]["issues"]["nodes"]:
        print(f"* :meeting:`{issue['title']} <{issue['iid']}>`")


if __name__ == "__main__":
    # modifications()
    meetings()
