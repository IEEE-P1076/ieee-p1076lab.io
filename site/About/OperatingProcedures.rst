Operating Procedures
####################

.. ATTENTION::
  This content is provided as a reference.
  It is not currently applied, because there is no active Project Authorization Request (PAR) at the moment.

* PDF approved by by IEEE-SA Standards Board on December 2010 and by VASG on 10-August-2011: `Operating Procedures for P1076 Working Group - also know as VHDL Analysis and Standardization Group (VASG) <http://www.eda-twiki.org/vasg/docs/p1076_wg_pp.pdf>`__
* Work in progress from 2015: `P1076_wg_individual_2015.doc <http://www.eda-twiki.org/twiki/pub/P1076/PublicDocuments/P1076_wg_individual_2015.doc>`__

1. Preface
**********

In today’s technological environment, standards play a critical role in product development and market competitiveness.
Responsibility for how a standard evolves begins in the working group.
Every input, behavior, and action has both a contributory and a potential legal consequence.
These procedures help protect working group participants and the IEEE by establishing the necessary framework for a
sound standardization process.

2. Modifications to these procedures
************************************

These Policies and Procedures outline the orderly transaction of business by the working group.
The working group may amend these procedures with the approval of its Sponsor.
The Sponsor may modify these procedures.
Modification in this context means that material in these procedures may be modified as long as that clause is not
indicated asone that cannot be changed.
It is strongly recommended that all subjects included in these procedures be addressed by the working group or Sponsor.
(See also clause 9.)

3. Hierarchy
************

The latest version of several documents takes precedence over these procedures in thefollowing order:

* New York State Not-for-Profit Corporation Law
* IEEE Certificate of Incorporation
* IEEE Constitution
* IEEE Bylaws
* IEEE Policies
* IEEE Board of Directors Resolutions
* IEEE Standards Association Operations Manual
* IEEE-SA Board of Governors Resolutions
* IEEE-SA Standards Board Bylaws
* IEEE-SA Standards Board Operations Manual
* IEEE-SA Standards Board Resolutions
* Policies/Procedures of the IEEE Computer Society (IEEE-CS)
* Policies/Procedures of the IEEE Computer Society Standards Activities Board (IEEE-CS SAB)
* Policies/Procedures of the DASCDASC Operations Manual
* Policies/Procedures of the Working Group (this document)
* Working Group Resolutions

*Robert's Rules of Order Newly Revised (RONR)* is the recommended guide on questionsof parliamentary procedure not
addressed in these procedures.

4. Fundamental principles of operation
**************************************

For the development of standards, openness and due process are mandatory.

Openness means that any person who has, or could be reasonably expected to have, a direct and material interest, and who
meets the requirements of these procedures has a right to participate by:

a) Attending working group meetings (in person or electronically)
b) Becoming a member of the working group
c) Becoming an officer of the working group
d) Expressing a position and its basis,
e) Having that position considered, and
f) Appealing if adversely affected.

IEEE due process requires a consensus of those parties interested in the project.
Consensus is defined as at least a majority agreement, but not necessarily unanimity.

Due process is based upon equity and fair play.
The standards development proces sshould strive to have both a balance of interests and not to be dominated by any
single interest category.

5. Working group responsibilities
*********************************

The working group shall

a) Complete the project from Project Authorization Request (PAR) approval to IEEE-SA Standards Board approval within the
   allotted time (normally 48 months)
b) Use the IEEE Standards document template format
c) Submit to the Sponsor any documentation required by the Sponsor; for example, aproject schedule or a monthly status
   report
d) Notify the Sponsor of the draft development milestones
e) Notify the Sponsor when the draft is ready to begin IEEE Standards Sponsor ballot

6. Officers
***********

6.1 Officer overview
====================

There shall be a Chair and a Secretary, and there should be a Vice-Chair.
The office of Treasurer is suggested if significant funds are involved in the operation of the working group and/or its
subgroups or if the group has multiple financial reports to supply to the IEEE Standards Association.
The chair shall be an IEEE member, individual IEEE-SA member, and DASC member.

Within six months of formation, the working group shall elect its operating officers in accordance with the procedures
of its Sponsor, and, where necessary, Robert’s Rules of Order.
Officers shall serve two year terms.

6.2 Election of officers
========================

The Chair or sponsor designee shall appoint an elections processor, whose function is to conduct an election.
The elections processor shall not be a nominee in the election and shall not vote in the election.
An election will seek to fill offices that are either vacant, have an official in temporary appointment, or when the
term of office has expired.

The working group members shall nominate to the elections processor one or more members for each office to be filled at
the election.
Nominees shall be eligible to hold the office for which they are elected.
A member shall not run for more than one office at the same time.
The response period for nominations shall be at least 14 calendar days.
If no nomination is received for an office, a temporary appointment shall be made in accordance with 6.3.

The elections processor shall conduct the election by letter ballot, electronic ballot or avote at a meeting.
If the vote is conducted by letter ballot or electronic ballot, voting willconclude no sooner than after 14 calendar
days.
Voting shall be by “approval”, whereby each balloter may cast one approval vote for each of any number of nominees for
an office.
The nominee with the greatest number of approval votes shall win the election, provided ballots are returned by a
majority of the eligible voters for that election.
If amajority of votes is not received, the ballot can be extended, or a new ballot will takeplace.
Any tie votes will be broken by a runoff ballot, where eligible voters may cast only one vote in the election.

The Sponsor shall affirm the election of the Chair and Vice-Chair.
If the sponsor does not affirm the Chair or Vice-Chair, another election will be run, or, the sponsor will make a
temporary appointment per clause 6.3.

6.3 Temporary appointments to vacancies
=======================================

If an office becomes vacant due to resignation, removal, lack of nomination at an election or for another reason, a
temporary appointment shall be made for a period of up to six months.
In the case of Chair or Vice-Chair, the Sponsor shall make the temporary appointment, with input from the working group.
In the case of Secretary, the Chair shall make the temporary appointment.
An appointment or election for the vacated office shall be conducted at the earliest practical time.

6.4 Removal of officers
=======================

An officer may be removed by approval of two-thirds of the members of the working group.
Removal of the Chair and Vice-Chair requires affirmation by the Sponsor.
Grounds for removal shall be included in any motion to remove an officer of the working group.
The officer suggested for removal shall be given an opportunity to make a rebuttal prior to the vote on the motion for
removal.

6.5 Responsibilities of working group officers
==============================================

6.5.1 Chair
-----------

The Chair or his or her designee shall

a) Lead the activity according to all of the relevant Policies and Procedures
b) Form Study Groups, as necessary
c) Be objective
d) Entertain motions, but not make motions
e) Not bias discussions
f) Delegate necessary functions
g) Ensure that all parties have the opportunity to express their views
h) Set goals and deadlines and adhere to them
i) Be knowledgeable in IEEE standards processes and parliamentary procedures andensure that the processes and procedures
   are followed
j) Seek consensus as a means of resolving issues
k) Prioritize work to best serve the group and its goals
l) Ensure compliance with the IEEE-SA Intellectual Property Policies, including but not limited to the IEEE-SA Patent
   Policy and Copyright Policy.
m) Fulfill any financial reporting requirements of the IEEE, in the absence of a Treasurer.
n) Participate as needed in meetings of the Sponsor to represent the working group

6.5.2 Vice-Chair(s)
-------------------

The Vice-Chair(s) shall

a) Carry out the Chair's duties if the Chair is temporarily unable to do so or chooses to recuse himself or herself
   (i.e., to give a technical opinion)
b) Be familiar with training materials available through IEEE Standards Development Online

6.5.3 Secretary
---------------

The Secretary shall

a) Distribute agendas at least 7 calendar days before a meeting
b) Record and have published minutes of each meeting within 60 calendar days of the end of the meeting
c) Create and maintain the membership roster
d) Record participant attendance at each meeting
e) Schedule and announce meetings in coordination with the Chair with at least 10 calendar days notice
f) Be responsible for the management and distribution of working group documentationin compliance with IEEE-SA
   guidelines, including but not limited to guidelines with regard to posting and distribution of drafts and approved
   IEEE standards.
g) Maintain lists of unresolved issues, action items, and assignments
h) Be familiar with training material available through IEEE Standards Development Online
i) Conduct electronic (email or web based) voting at the request of the Chair and report the results to the working
   group.

If a Secretary is not elected by the working group, the chair or the chair's designee shall carry out these
responsibilities.

6.5.4 Treasurer
---------------

The Treasurer shall

a) Maintain a budget
b) Control all funds into and out of the working group’s bank account
c) Follow IEEE policies concerning standards meetings and finances
d) Adhere to the IEEE Financial Operations Manual

If a Treasurer is not elected by the working group, the chair or the chair's designee shall carry out these
responsibilities.

7. Working group
****************

7.1 Overview
============

Working group membership is by individual.
Those attending meetings shall pay any required meeting fees if established.
Participants seeking working group membership are responsible for fulfilling the requirements to gain and maintain
membership.

7.2 Working group membership status
===================================

Since the working group has international participation, meeting times may end up being at an unreasonable time for some
participants.
As a result, rather than base membership on meeting attendance, membership will be based on participation in the working
group (either in meeting or on the reflector).
Participation will be by formally tracked by tracking voting in working group electronic (email or web-based) votes.

All official working group votes will be done by electronically (either email-based or web-based).
Any voting during a meeting shall be subject to confirmation by the working group via electronic vote.
Each member is expected to vote in electronic votes as required by these procedures.

Membership, is granted automatically to those participants voting in the first vote of a newly chartered working group.
Membership status is maintained through consistent participation in working group votes.
A non-member who votes in 2 of the previous 3 votes will gain Membership status for next vote.
A member who does not vote in 2 of the previous 3 votes will lose their membership status.
Non-member's votes are not counted towards the outcome of the vote in any way - they merely indicate participation.

Voting records shall be maintained by the secretary and posted with the vote results.

7.3 Subgroups of the working group
==================================

The working group may, from time to time, form subgroups for the conduct of its business.
Such formation shall be explicitly noted in an official record, such as meeting minutes.
At the time of formation, the working group shall determine the scope and duties delegated to the subgroup.
Voting rights within such subgroups shall be defined.
Any changes to its scope and duties will require the approval of the working group.
Any resolution of a subgroup shall be subject to confirmation by the working group.

The Chair of the working group shall appoint the chair of the subgroup.

8. Working group roster and membership list
*******************************************

8.1 Working group roster
========================

A working group roster is a vital aspect of standards development.
It serves as a record of members and participants in the working group and is an initial tool if an issue of
indemnification arises during the process of standards development.

A working group officer or designee shall maintain a current and accurate roster of members and participants in the
working group.
The roster shall include at least the following:

a) Title of the Sponsor and its designation
b) Title of the working group and its designation
c) Officers--Chair, Vice-Chair, Secretary, Treasurer (as applicable)d) Members and participants (including names, email
   addresses, and affiliations for all members)

A copy of the working group roster shall be supplied to the IEEE Standards Association at least annually by a working
group officer or designee.
Due to privacy concerns, the roster shall not be distributed, except to the IEEE-SA staff, IEEE-SA Board of Governors
and IEEE-SA Standards Board, unless all Working Group members and participants have submitted their written approval for
such distribution.

8.2 Working group member list
=============================

A working group officer or designee shall maintain a current and accurate membership list.
The membership list can be posted on the committee web site and can be publically distributed.
The membership list shall be limited to the following:

a) Title of the Working Group and its designation
b) Scope of the Working Group
c) Officers: Chair, Vice-Chair, Secretary (Treasurer)
d) Members: for all, name, affiliation

9. Voting
*********

9.1 Approval of an action
=========================

Approval of an action listed in 9.2 requires an approval by a majority vote.
Approval of an action listed in 9.3 requires approval by a two-thirds vote.
Two types of votes are described.

a) At a meeting (including teleconferences) where quorum has been established, approval ratio is calculated as Approve
   votes divided by the sum of Approve and Do Not Approve votes.
b) Outside of a meeting (e.g. letter ballot), approval ratio is calculated as Approve votes divided by the sum of
   Approve and Do Not Approve notes.
   A majority of all voting members of the Committee must respond for the ballot to be valid.

9.2 Actions requiring approval by a majority
============================================

The following actions include approval by a majority vote

a) Adoption of working group procedures or revisions thereof
b) Formation of a subgroup, including its procedures, scope, and duties
c) Disbandment of subgroups when no other provisions to disband are in place or prior to its completion.
d) Approval of minutes.
e) Any other working group action not documented in 9.3

9.3 Actions requiring approval by a supermajority
=================================================

The following actions require approval by a two-thirds vote:

a) Approval of change of the working group scope
b) Approval to move the draft standards project to the Sponsor for IEEE Standards Sponsor ballot

These actions are subject to confirmation by the Sponsor.

9.4 Voting between meetings
===========================

The working group shall be allowed to conduct votes between meetings at the discretion of the Chair by use of a letter
or electronic ballot.
If such actions are to be taken, they shall follow the rules of IEEE Bylaw I-300.4(4).

The minimum period for an electronic ballot, other than for officer elections, shall be atleast 7 days.

9.5 Quorum
==========

The presence of a quorum must be announced by the Chair at the beginning of each meeting.
Unless otherwise approved by the Sponsor, a quorum shall be defined as one-half of the working group members.
If a quorum is not present actions may be taken subsequent to confirmation by a letter or electronic ballot as detailed
in 9.4, or at the nextworking group meeting

10. Meetings
************

Working Group meetings shall be held, as decided by the working group, the Chair, or by petition of 15% or more of the
members, to conduct business, such as making assignments, receiving reports of work, considering draft standards, and
considering views and objections from any source.

A working group meeting shall be announced, by a working group officer or designee, 10 calendar days in advance to all
participants.
An agenda shall be distributed at least 7 calendar days in advance of a meeting.

The working group, or meeting host, may charge a meeting fee to cover services needed for the conduct of the meeting.
The fee shall not be used to restrict participation by any interested parties.

While having a balance of all interested parties is not an official requirement for a working group, it is a desirable
goal.
As such, the officers of the working group should consider issues of balance and dominance that may arise and discuss
them with the Sponsor.

Participants shall be asked to state their employer and affiliation at each working group meeting as required by the
SASB Operations Manual (Section 5.3.3.1 Disclosure of Affiliation).

11. Conduct
***********

It is expected that participants in the working group behave in a professional manner at all times.
Participants shall demonstrate respect and courtesy towards officers and each other, while allowing participants a fair
and equal opportunity to contribute to the meeting, in accordance with the IEEE Code of Ethics.
All working group participants shall act in accordance with all IEEE Standards policies and procedures.
Where applicable, working group participants shall comply with IEEE Policies Section 9.8 on Conflict of Interest.

12. Appeals
***********

The working group recognizes the right of appeal.
If technical or procedural appeals are referred back to the working group, every effort should be made to ensure that
impartial handling of complaints regarding any action or inaction on the part of the working group is performed in an
identifiable manner.

If the working group must conduct an appeal hearing, it shall model its appeals process based on the appeals processes
of the IEEE-SA Standards Board.

13. Communications
******************

Formal inquiries relating to the working group should be directed to the Chair and recorded by the Secretary.
All replies to such inquiries shall be made through the Chair.
These communications shall make it clear that they are responses from the working group.
