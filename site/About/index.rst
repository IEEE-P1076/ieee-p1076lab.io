.. _About:

About
#####

.. _About:Admin:

Working Group Administration
============================

Working group officers:

* *Chair*: Jim Lewis (`JimLewis@eda-twiki <http://www.eda-twiki.org/cgi-bin/view.cgi/Main/JimLewis>`__, `JimLewis@gitlab <https://gitlab.com/JimLewis>`__)
* *Vice Chair*: Patrick Lehmann (`PatrickLehmann@eda-twiki <http://www.eda-twiki.org/cgi-bin/view.cgi/Main/PatrickLehmann>`__, `paebbels@gitlab <https://gitlab.com/paebbels>`__)
* *Vice Chair*: Rob Gaddi (`RobGaddi@eda-twiki <http://www.eda-twiki.org/cgi-bin/view.cgi/Main/RobGaddi>`__, `rgaddi@gitlab <https://gitlab.com/rgaddi>`__)
* *Secretary*: Unai Martinez-Corral (`UnaiCorral@eda-twiki <http://www.eda-twiki.org/cgi-bin/view.cgi/Main/UnaiCorral>`__, `umarcor@gitlab <https://gitlab.com/umarcor>`__)

Documents:

* `Working Group Roster <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/VhdlRoster>`__ - private to working group - login first
* `Voting Information <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/VotingRecords>`__
* `2008-2019 IEEE Approved Project Authorization Request (PAR) <http://www.eda-twiki.org/vasg/docs/p1076_IEEE_approved_par.pdf>`__
* IEEE:

  * Patent Policy - `IEEE Slides <https://development.standards.ieee.org/myproject/Public/mytools/mob/slideset.pdf>`__ - in `IEEE Bylaws <http://standards.ieee.org/develop/policies/bylaws/sect6-7.html>`__
  * `Antitrust Policy <http://standards.ieee.org/develop/policies/antitrust.pdf>`__

.. toctree::
    :maxdepth: 1

    OperatingProcedures

How to add users
----------------

How to add someone to:

* Mail reflector/list.
* `TWIKI <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/HowToAddToTwiki>`__.
* GitLab.

.. _About:Standards:

VHDL Standards
==============

VHDL standards are available through `IEEE standards association <http://shop.ieee.org/>`__.

* IEEE 1076:

  * `1076-2019 <https://ieeexplore.ieee.org/document/8938196>`__.
  * `1076-2008 <https://ieeexplore.ieee.org/document/4772740>`__ aka IEC 61691-1-1:2011.
  * `1076-2002 <https://ieeexplore.ieee.org/document/1003477>`__ aka IEC 61691-1-1 Ed.1 (2004-10).
  * `1076-2000 <https://ieeexplore.ieee.org/document/893288>`__.
  * `1076-1993 <https://ieeexplore.ieee.org/document/392561>`__.
  * `1076-1987 <https://ieeexplore.ieee.org/document/26487>`__.

* `IEEE 1076.1 <http://www.eda-twiki.org/cgi-bin/view.cgi/P10761/WebHome>`__ VHDL Analog and Mixed-Signal (VHDL-AMS):

  * 1076.1.1: VHDL Analog and Mixed-Signal Extensions---Packages for Multiple Energy Domain Support (stdpkgs)

* IEEE 1076.2: VHDL Math Package (``math_real``) (part of 1076 starting with 1076-2008)
* IEEE 1076.3: VHDL Synthesis Package (vhdlsynth) (``numeric_std``) (fphdl) (part of 1076 starting with 1076-2008)
* IEEE 1076.4: Timing (VHDL Initiative Towards ASIC Libraries: VITAL)
* IEEE 1076.6: VHDL RTL Synthesis 2004, 1999 (deprecated by DASC)
* IEEE 1164: VHDL Multivalue Logic Packages (``std_logic_1164``) (part of 1076 starting with 1076-2008)
