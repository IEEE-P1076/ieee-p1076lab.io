Prior work
##########

VHDL-2019
=========

Some of these lists are redundant - but better that than miss something

* `List of language changes for VHDL-2019 <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/VHDL2017>`__
* `Proposals & Requirements List <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl2019CollectedRequirements>`__
* `Package Updates for 1076-2017 including: FIxed and floating point packages <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl2019PackageUpdates>`__
* `Meeting Minutes from 2019 <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl2019MeetingMinutes>`__
* `Issue Screening and Analysis (ISAC) <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/IssueScreening>`__
* `Working Group Status <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl2019WorkingGroupStatus>`__
* `Active IR From ISAC <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl2019ActiveIRTwiki>`__
* `Action Item List <http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/Vhdl2019ActionItems>`__
* `Submit an enhancement request or a bug <https://bugzilla.mentor.com/>`__

VHDL-2008 and Prior
===================

* `Original VASG webpage <http://www.eda-twiki.org/vasg>`__
* `VHDL-200X-FT webpages <http://www.eda-twiki.org/vhdl-200x/vhdl-200x-ft/proposals/proposals.html>`__
* `VHDL-2008 Accellera Docs <http://www.accellera.org/apps/group_public/documents.php?view=>`__
* `VHDL-2008 Accellera Member Docs <http://www.accellera.org/apps/org/workgroup/vhdl/documents.php?folder_id=0>`__
